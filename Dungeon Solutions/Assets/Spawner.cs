﻿using UnityEngine;
using System.Collections;
using System;

public class Spawner: MonoBehaviour {
    [SerializeField]
    public string componentType;

    public void Start()
    {
        gameObject.AddComponent(Type.GetType(componentType));

        Destroy(this);
    }

	
}
