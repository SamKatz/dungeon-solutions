﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Weapon : MonoBehaviour
{
    protected float damage;
    protected float knockback;
    protected List<Entity> hit = new List<Entity>();

    
    public static GameObject spawnWeapon<T>(WeaponDetails core, GameObject parent, InitializeDelegate init = null) where T : Weapon
    {

        Vector2 dir = parent.GetComponent<Player>().getShootDirection();

        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis((angle+345)%360, Vector3.forward);

        GameObject obj = new GameObject();
        obj.transform.parent = parent.transform;
        obj.transform.position = new Vector3(parent.transform.position.x + (2 * dir.x), parent.transform.position.y + (2 * dir.y),0);
        obj.transform.rotation = rotation;
        obj.AddComponent<T>();
        obj.name = "Weapon " + core.spriteloc + "_" + core.damage;
        if (init != null)
        {
            init();
        }
        obj.GetComponent<T>().damage = core.damage;
        obj.GetComponent<T>().knockback = core.knock;


        obj.transform.localScale = new Vector3(core.getWeaponScalar()[0]/parent.transform.localScale.x,
                                            core.getWeaponScalar()[1] / parent.transform.localScale.y, 1);

        SpriteRenderer sr = obj.AddComponent<SpriteRenderer>();

        sr.sprite = Resources.Load<Sprite>(core.spriteloc);

        BoxCollider2D c2d = obj.AddComponent<BoxCollider2D>();
        c2d.size = sr.sprite.bounds.size;
        c2d.isTrigger = true;

        return obj;
    }

    public delegate void InitializeDelegate();



    void OnTriggerEnter2D(Collider2D collision)
    {

        Entity ent = collision.gameObject.GetComponent<Entity>();
        if (ent != null && !ent.gameObject.transform.Equals(this.transform.parent) && !hit.Contains(ent))
        {
            ent.damage((int)damage);
            Vector2 dir = new Vector2(this.transform.position.x - ent.gameObject.transform.position.x, this.transform.position.y - ent.gameObject.transform.position.y);
            dir = dir.normalized;
            ent.knockback(-1 * dir, knockback);
            hit.Insert(0, ent);
        }
    }

    void Start()
    {
        Destroy(this.gameObject, 0.5f);
    }

    protected virtual void Update()
    {
              
    }
}
