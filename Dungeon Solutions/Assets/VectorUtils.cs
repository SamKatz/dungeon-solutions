﻿using UnityEngine;
using System.Collections;

public class VectorUtils {

	public static Vector2 to2d(Vector3 vec)
    {
        return new Vector2(vec.x, vec.y);
    }

    public static Vector3 to3d(Vector2 vec)
    {
        return new Vector3(vec.x, vec.y, 0);
    }

    public static float ManhattanDist(Vector2 v1, Vector2 v2)
    {
        return Mathf.Abs(v1.x - v2.x) + Mathf.Abs(v1.y - v2.y);
    }
}
