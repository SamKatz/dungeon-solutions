﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using System;

public abstract class Enemy : Entity {


    
    [SerializeField]
    protected Entity target;
    [SerializeField]
    protected bool awake = false;

    Seeker seeker;

    protected override void Start()
    {
        base.Start();
        seeker = this.gameObject.AddComponent<Seeker>();
        Debug.Log("Added seeker");
    }

    public virtual Entity getTarget()
    {
        return this.target;
    }

    public virtual void setTarget(Entity target)
    {
        this.target = target;
    }

    public virtual void selectTarget()
    {
        Player closest = getClosestPlayer();
        this.lastTargetUpdate = Time.fixedTime;
        if (closest == null) return;
        setTarget(closest);
        
    }

    public Player getClosestPlayer()
    {
        Player closest = null;
        float closestDistance = float.MaxValue;

        foreach (Player p in Player.players)
        {
            float distance = Vector2.Distance(this.transform.position, p.transform.position);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closest = p;
            }
        }
        return closest;
    }
    protected Path lastPath;
    protected float lastPathUpdate = -1000;
    protected float lastTargetUpdate = -1000;
    [SerializeField]
    protected int lastWaypointReached = -1;

    protected override void Update()
    {
        base.Update();

        if(lastPath != null)
        {
            while (lastWaypointReached < lastPath.vectorPath.Count - 1 && Vector2.Distance(this.transform.position, lastPath.vectorPath[lastWaypointReached + 1]) < 3)
            {
                lastWaypointReached++;
            }
            if (lastWaypointReached >= lastPath.vectorPath.Count - 1)
            {
                lastPath = null;
            }
            else
            {
                Vector2 dir = lastPath.vectorPath[lastWaypointReached + 1] - this.transform.position;
                if (Vector2.Distance(this.transform.position, target.transform.position) < 3.5)
                {
                    this.move(Vector2.zero);
                }else
                {
                    this.move(dir);
                }
                
            }
        }


        if(lastTargetUpdate < Time.fixedTime + 1.0)
        {
            if (!awake)
            {
                awake = AIUtils.canISeePlayer(this);
                lastTargetUpdate = Time.fixedTime;
            }
            else
            {
                this.selectTarget();
            }
        }

        if(Time.fixedTime > lastPathUpdate + 0.25 && target != null)
        {
            seeker.StartPath(this.transform.position, target.transform.position, OnPathComplete);
            this.lastPathUpdate = Time.fixedTime;
        }


        if (!this.canIMove()) return;
        if (this.hasCondition<ConditionManager.Terrified>())
        {
            //TODO: Make this behavior more rational and less derpy
            Vector2 fleedir = UnityEngine.Random.insideUnitCircle;
            this.move(fleedir.normalized);


        }

        

    }



    protected void OnPathComplete(Path p)
    {
        if (p.error)
        {
            Debug.Log("Pathfinding error occurred");
            return;
        }
        this.lastWaypointReached = -1;
        this.lastPath = p;

    }

	
}
