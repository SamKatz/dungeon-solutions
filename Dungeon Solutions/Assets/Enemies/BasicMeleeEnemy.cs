﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasicMeleeEnemy : Enemy {

	// Use this for initialization
	protected override void Start () {
        base.Start();
        this.speed = 15;
        this.projectileSpawnDistance = 1.5f;
        this.hitPointsMAX = 30;
        this.hitPoints = 30;

        SpriteRenderer sr = this.gameObject.GetComponent<SpriteRenderer>();
        sr.sprite = Resources.Load<Sprite>("TestResources/Circle_Orange");

        Rigidbody2D r2d = this.gameObject.GetComponent<Rigidbody2D>();
        r2d.freezeRotation = true;

        BoxCollider2D b2d = this.gameObject.AddComponent<BoxCollider2D>();
        b2d.size = new Vector2(0.32f, 0.32f);

        this.gameObject.transform.localScale = new Vector3(5, 5, 1);

    }



    protected float lastAttack = -1000;


	// Update is called once per frame
	protected override void Update () {
        base.Update();
        if(lastAttack + 3 < Time.fixedTime)
        {
            Player closest = this.getClosestPlayer();
            if (Vector2.Distance(closest.transform.position, this.transform.position) > 5)
            {
                return;
            }

            this.lastAttack = Time.fixedTime;

            Vector2 dir;

            GameObject obj = new GameObject();

            dir = closest.transform.position - this.transform.position;

            dir.Normalize();

            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis((angle + 345) % 360, Vector3.forward);


            obj.transform.parent = this.transform;
            obj.transform.position = new Vector3(this.transform.position.x + (2 * dir.x), this.transform.position.y + (2 * dir.y), 0);
            obj.transform.rotation = rotation;
            obj.AddComponent<Sword>();
            obj.name = "Enemy Sword";

            obj.GetComponent<Sword>().damage = 10;
            obj.GetComponent<Sword>().knockback = 1;


            obj.transform.localScale = new Vector3(1, 1, 1);

            SpriteRenderer sr = obj.AddComponent<SpriteRenderer>();

            sr.sprite = Resources.Load<Sprite>("testresources/testsword");

            BoxCollider2D c2d = obj.AddComponent<BoxCollider2D>();
            c2d.size = sr.sprite.bounds.size;
            c2d.isTrigger = true;

        }
	}

    public class Sword : MonoBehaviour
    {
        public int damage;
        public float knockback;
        protected List<Entity> hit = new List<Entity>();




        void OnTriggerEnter2D(Collider2D collision)
        {

            Entity ent = collision.gameObject.GetComponent<Entity>();
            if (ent != null && !ent.gameObject.transform.Equals(this.transform.parent) && !hit.Contains(ent))
            {
                ent.damage(damage);
                Vector2 dir = new Vector2(this.transform.position.x - ent.gameObject.transform.position.x, this.transform.position.y - ent.gameObject.transform.position.y);
                dir = dir.normalized;
                ent.knockback(-1 * dir, knockback);
                hit.Insert(0, ent);
            }
        }

        void Start()
        {
            Destroy(this.gameObject, 0.5f);
        }

        protected virtual void Update()
        {

            this.transform.Rotate(0, 0, -1 * Time.deltaTime * 60);
            this.transform.RotateAround(this.transform.parent.position, Vector3.forward, -2 * Time.deltaTime * 60);


        }
    }
}
