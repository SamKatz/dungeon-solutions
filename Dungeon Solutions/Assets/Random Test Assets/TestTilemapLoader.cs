﻿using UnityEngine;
using System.Collections;
using SpriteTile;
using Pathfinding;
using System;
using System.Collections.Generic;

public class TestTilemapLoader : MonoBehaviour {
    public TextAsset level;

    public static bool doLava = false;

    public static int lavaNo = 1;
    // Use this for initialization
    void Start () {
        Tile.SetCamera();
        Tile.LoadLevel(level);

        //assumes square tiles, because rectangular tiles are frickin' weird
        int levelwidth = (int) Math.Ceiling(Tile.GetTileSize().x * Tile.GetMapSize().x);
        int levelheight = (int) Math.Ceiling(Tile.GetTileSize().y * Tile.GetMapSize().y);

        // This holds all graph data
        AstarData data = AstarPath.active.astarData;
        // This creates a Grid Graph

        GridGraph gg = (GridGraph) data.graphs[0];
        //GridGraph gg = data.AddGraph(typeof(GridGraph)) as GridGraph;
        // Setup a grid graph with some values



        gg.width = levelwidth;
        gg.depth = levelheight;
        gg.nodeSize = 1;
        gg.center = new Vector3(levelwidth/2-1+levelwidth%2, levelheight/2-1+levelwidth%2, 0);
        gg.rotation = new Vector3(90, 0, 0);

        GameObject colliders = GameObject.Find("SpriteTileColliders");
        colliders.layer = 8;
        foreach (Transform child in colliders.transform)
        {
            child.gameObject.layer = 8;
        }

        // Updates internal size from the above values
        gg.UpdateSizeFromWidthDepth();
        // Scans all graphs, do not call gg.Scan(), that is an internal method
        AstarPath.active.Scan();

        WeaponDetails wd = new WeaponDetails(WeaponDetails.WeaponType.Sword, false, 30, 20, "TestResources/testsword", WeaponDetails.Rarity.Rare);
        WeaponDetails wd2 = new WeaponDetails(WeaponDetails.WeaponType.Wand, true, 10, 40, "TestResources/Copper_Broadsword", WeaponDetails.Rarity.Uncommon);
        AbilityContainer.createAbilityContainer(new SwordAbility(null, wd), 30, 80);
        AbilityContainer.createAbilityContainer(new RangedAbility(null, wd2), 40, 80);

        //SpawnEnemies();

    }

    void SpawnEnemies()
    {
        for(int i = 0; i < 30; i++)
        {
            int x = UnityEngine.Random.Range(0, Tile.GetMapSize().x-1);
            int y = UnityEngine.Random.Range(0, Tile.GetMapSize().y-1);
            SpriteTile.Int2 loc = new SpriteTile.Int2(x, y);
            if (!Tile.GetCollider(loc))
            {
                int type = UnityEngine.Random.Range(0, 2);
                Debug.Log("picked type " + type);

                GameObject go = new GameObject();
                switch (type) {
                    case 0:  go.AddComponent<ArcherEnemy>();
                        break;
                    case 1:
                        go.AddComponent<StupidEnemy>();
                        break;
                }
                go.transform.position = Tile.MapToWorldPosition(loc);
                if (AIUtils.canISeePlayer(go.GetComponent<Entity>()))
                {
                    Destroy(go);
                    i--;
                    continue;
                }


                continue;
                
            }else
            {
                i--;
                continue;
            }

        }
    }

    void StartLava()
    {
        int i = 0;
        
        while(!Tile.GetCollider(new SpriteTile.Int2(i, i))){
            i++;
        }
        Tile.SetTile(new SpriteTile.Int2(i, i), 0, lavaNo);

    }
    float lastSpread = 0;

    // Update is called once per frame
    void FixedUpdate () {
        if (doLava && Time.fixedTime - lastSpread > 1)
        {
            this.lastSpread = Time.fixedTime;
            HashSet<SpriteTile.Int2> toSpreadTo = new HashSet<SpriteTile.Int2>(); //replace with GetTilePositions?

            for(int i = 0; i < Tile.GetMapSize().x; i++)
            {
                for(int j = 0; j < Tile.GetMapSize().y; j++)
                {
                    SpriteTile.Int2 loc = new SpriteTile.Int2(i, j);
                    if (Tile.GetTile(loc).tile == lavaNo)
                    {
                        SpriteTile.Int2 north = new SpriteTile.Int2(i, j+1);
                        SpriteTile.Int2 south = new SpriteTile.Int2(i, j - 1);
                        SpriteTile.Int2 west = new SpriteTile.Int2(i-1, j);
                        SpriteTile.Int2 east = new SpriteTile.Int2(i+1, j);
                        toSpreadTo.Add(north);
                        toSpreadTo.Add(south);
                        toSpreadTo.Add(west);
                        toSpreadTo.Add(east);

                        
                    }
                }
            }

            foreach(SpriteTile.Int2 loc in toSpreadTo)
            {
                if (loc.x >= 0 && loc.y >= 0 && loc.x <= Tile.GetMapSize().x - 1 && loc.y <= Tile.GetMapSize().y && !Tile.GetCollider(loc))
                {
                    Tile.SetTile(loc, 0, lavaNo);
                }
            }
        }
        List<Entity> toBurn = new List<Entity>();
        foreach(Entity e in Entity.entities)
        {
            if (isOnLava(e))
            {
                toBurn.Add(e);
                

                
            }
        }

        foreach(Entity e in toBurn)
        {
            if (!e.hasCondition<ConditionManager.OnFire>())
            {
                ConditionManager.OnFire fire = new ConditionManager.OnFire(Time.fixedTime + 5, 10);
                e.addCondition(fire);
            }
            else
            {
                foreach (ConditionManager.Condition c in e.getConditions<ConditionManager.OnFire>())
                {
                    c.expirationTime += Time.fixedDeltaTime;
                }
            }
        }

        



    }

    public static bool isOnLava(Entity e) //TODO: This is not accurate, update
    {
        if(Tile.GetTile(e.transform.position).tile == lavaNo)
        {
            return true;

        }


        return false;
    }
}
