﻿using UnityEngine;
using System.Collections;
//Note this line, if it is left out, the script won't know that the class 'Path' exists and it will throw compiler errors
//This line should always be present at the top of scripts which use pathfinding
using Pathfinding;
public class ArcherEnemy : Entity
{
    //The point to move to
    private Seeker seeker;

    Player target;

    //The calculated path
    public Path path;
    //The AI's speed per second
    //The max distance from the AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance = 2f;
    //The waypoint we are currently moving towards
    public int currentWaypoint = 0;

    public WeaponDetails wd2 = new WeaponDetails(WeaponDetails.WeaponType.Wand, true, 20, 40, "TestResources/Copper_Broadsword", WeaponDetails.Rarity.Uncommon);

    int projectileMask = 0;
    bool awake = false;
    protected override void Start()
    {
        base.Start();
        

        this.name = "Archer Enemy";

        this.speed = 5;

        projectileMask = LayerMask.GetMask("Level Geometry", "Entities");

        this.setHPMax(50);
        this.setHP(50);

        this.projectileSpawnDistance = 2;

        SpriteRenderer sr = this.gameObject.GetComponent<SpriteRenderer>();
        sr.sprite = Resources.Load<Sprite>("TestResources/Circle_Orange");

        Rigidbody2D r2d = this.gameObject.GetComponent<Rigidbody2D>();
        r2d.freezeRotation = true;

        BoxCollider2D b2d = this.gameObject.AddComponent<BoxCollider2D>();
        b2d.size =new Vector2(0.32f, 0.32f);

        this.gameObject.transform.localScale = new Vector3(5, 5, 1);

        seeker = this.gameObject.AddComponent<Seeker>();

        SimpleSmoothModifier ssm = this.gameObject.AddComponent<SimpleSmoothModifier>();
        ssm.smoothType = SimpleSmoothModifier.SmoothType.Simple;
        ssm.uniformLength = true;
        ssm.maxSegmentLength = 1.5f;
        ssm.iterations = 2;
        ssm.strength = 0.15f;

        //Debug.Log("Mask = " + this.projectileMask);
        //Start a new path to the targetPosition, return the result to the OnPathComplete function

    }
    protected void OnPathComplete(Path p)
    {

        if (!p.error)
        {
            //Debug.Log("Yay, we got a path back. Did it have an error? " + p.error);
            path = p;

            foreach (Vector3 loc in p.vectorPath)
            {
                //Debug.Log(loc.x + " " + loc.y);
            }
            //Reset the waypoint counter
            currentWaypoint = 0;
        }
        else
        {
            //Debug.Log("Yay, we got a path back. Did it have an error? " + p.error);
        }
    }

    float lastPathUpdate = -100;
    float lastFired = -100;
    protected override void Update()
    {
        base.Update();
        if (!awake)
        {
            if (Time.fixedTime - lastPathUpdate > 1f)
            {
                //Debug.Log("Can I see the player?");
                if (AIUtils.canISeePlayer(this))
                {
                    //Debug.Log("Forward on the foe!");
                    awake = true;
                }else
                {
                    this.lastPathUpdate = Time.fixedTime;
                    return;
                }
            }else
            {
                return;
            }
                
        }

        

        if (Time.fixedTime - lastPathUpdate > 1f)
        {
            findTarget();
        }

        if (path == null)
        {
            //We have no path to move after yet
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            currentWaypoint = path.vectorPath.Count - 1;

            Debug.Log(currentWaypoint + " _ " + path.vectorPath.Count);

        }

        while (currentWaypoint < path.vectorPath.Count - 1 && Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
        {
            currentWaypoint++;
            //Debug.Log("Found waypoint - changing target to " + path.vectorPath[currentWaypoint].x + " " + path.vectorPath[currentWaypoint].y);

        }
        


        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        dir *= speed;
        
        this.move(dir);
        
        

        









        //Direction to the next waypoint



        Vector2 vel = (target.transform.position - this.transform.position);
        vel.Normalize();
        Vector3 startpos = this.transform.position;
        startpos += new Vector3(vel.x * 3, vel.y * 3, 0);
        //12582912 - include layers 8 and 9, Level Geometry and Entities
        if (Time.fixedTime - lastFired > 0.5)
        {
            RaycastHit2D rh2d = Physics2D.Raycast(startpos, vel, 100, this.projectileMask);


            if (rh2d.collider == null || rh2d.collider.gameObject.GetComponent<Player>() == null)
            {
                return;

            }
            else
            {
                //Debug.Log("About to shoot " + rh2d.collider.gameObject.name);
            }

            






            vel *= 22;

            if (this.canIAttack())
            {
                lastFired = Time.fixedTime;
                this.move(new Vector2(0, 0));
                attack(wd2);
            }


        }





    }

    public void findTarget()
    {

        
        if (Player.players.Count == 0) return;


        lastPathUpdate = Time.fixedTime;

        float distance = float.MaxValue;
        foreach (Player p in Player.players)
        {
            if ((p.transform.position - this.transform.position).sqrMagnitude < distance)
            {
                target = p;
                distance = (p.transform.position - this.transform.position).sqrMagnitude;
            }
        }

        seeker.StartPath(this.transform.position, target.transform.position, OnPathComplete);
        //Debug.Log(target.transform.position.x + " " + target.transform.position.y);

    }

    protected override void onDeath()
    {
        base.onDeath();
        //DroppedWeapon.dropNewWeapon(wd2, this.transform.position);

    }
}



/*
 * if (distance.magnitude < 10 && Time.fixedTime - lastFired > 2)
        {
            lastFired = Time.fixedTime;

            
            Vector2 vel = distance;
            vel.Normalize();
            Vector3 startpos = this.transform.position;
            startpos += new Vector3(vel.x*3, vel.y*3, 0);

            vel *= 2;
            Projectile.InitializeDelegate talker = new Projectile.InitializeDelegate(talk);
            Projectile.spawnProjectile<Projectile>(5, "TestResources/wildebeest_transparent_sprite_by_milfeyu-d472yim", startpos, vel, init:talker);

            
        }
        distance.Normalize();


        this.rigidBody.velocity = distance * 0.5f;
*/
