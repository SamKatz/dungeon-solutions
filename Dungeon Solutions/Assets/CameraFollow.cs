﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    [SerializeField]
	public GameObject[] follow;
    private Camera cam;
	// Use this for initialization
	void Start () {
        cam = this.gameObject.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        

        float minx = float.MaxValue;
        float maxx = float.MinValue;
        float miny = float.MaxValue;
        float maxy = float.MinValue;

        foreach(GameObject go in follow)
        {
            minx = Mathf.Min(minx, go.transform.position.x);
            miny = Mathf.Min(miny, go.transform.position.y);
            maxx = Mathf.Max(maxx, go.transform.position.x);
            maxy = Mathf.Max(maxy, go.transform.position.y);
        }

        float pad = 10;

        Rect bound = Rect.MinMaxRect(minx-pad, miny-pad, maxx+pad, maxy+pad);
        this.transform.position = bound.center;
        this.transform.Translate(0, 0, -15);

        this.cam.orthographicSize = Mathf.Max(Mathf.Abs(bound.height/2), Mathf.Abs(bound.width / cam.aspect / 2));

    }
}
