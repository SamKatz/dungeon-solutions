﻿using UnityEngine;
using System.Collections;

public class RangedWeapon : MonoBehaviour {

    public void Start()
    {
        Destroy(this.gameObject, 0.5f);
    }

    public static GameObject spawnWeapon<T>(WeaponDetails core, bool isPlayer, GameObject parent, InitializeDelegate init = null) where T : RangedWeapon
    {

        Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Vector2 dir;
        if (isPlayer)
            dir = parent.GetComponent<Player>().getShootDirection();
        else
        {
            Player closest = null;
            float closestDistance = float.MaxValue;
            foreach (Player p in Player.players)
            {
                if (Vector2.Distance(p.transform.position, parent.transform.position) < closestDistance)
                {
                    closestDistance = Vector2.Distance(p.transform.position, parent.transform.position);
                    closest = p;
                }
            }
            if (closest == null) Debug.Log("no players to attack");
            dir = new Vector2(closest.transform.position.x - parent.transform.position.x, closest.transform.position.y - parent.transform.position.y);
        }
        dir.Normalize();

        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis((angle) % 360, Vector3.forward);

        GameObject obj = new GameObject();
        obj.transform.parent = parent.transform;
        obj.transform.position = new Vector3(parent.transform.position.x + (2 * dir.x), parent.transform.position.y + (2 * dir.y), 0);

        obj.transform.rotation = rotation;
        obj.transform.Rotate(new Vector3(0, 0, -90));

        obj.AddComponent<T>();
        obj.name = "Weapon " + core.spriteloc + "_" + core.damage;
        if (init != null)
        {
            init();
        }


        obj.transform.localScale = new Vector3(core.getWeaponScalar()[0] / parent.transform.localScale.x,
                                            core.getWeaponScalar()[1] / parent.transform.localScale.y, 1);

        SpriteRenderer sr = obj.AddComponent<SpriteRenderer>();

        sr.sprite = Resources.Load<Sprite>(core.spriteloc);

        int speed = 18;
        if (isPlayer) speed = 36;

        GameObject pro = Projectile.spawnProjectile<Projectile>(
            (int) core.damage, "TestResources/Fire", 6,
            VectorUtils.to2d(obj.transform.position) + (dir * parent.GetComponent<Entity>().getProjectileSpawnDistance()),
            dir * speed,
            5.0f //this prevents ranged kb from working
            );

        pro.transform.Rotate(new Vector3(0, 0, 90));

        return obj;
    }

    public delegate void InitializeDelegate();

    protected virtual void Update()
    {
       
    }
}
