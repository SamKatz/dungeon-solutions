﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Player : Entity 
{
    public Vector2 force;

    public AbilityPanelManager apm;

    // colors to eventually be swapped for sprites, using SpriteRender. Too lazy to create 8 sprites.
	enum colors {red=1,orange,yellow,green,blue,purple=8,pink=10,gray=12};

    public static List<Player> players = new List<Player>();

    [SerializeField]
    public string playerPrefix;


    [SerializeField]
    public string classname;

    

	protected override void Awake()
    {
        base.Start();
        this.speed = 10f;
        players.Add(this);
        Debug.Log("Player registered.");


        base.Start();

    }

    protected override void Start()
    {
        
    }

    protected override void onDeath()
    {
        base.onDeath();
        Application.Quit();
    }

    public override void damage(int dmg)
    {
        setHP(getHP() - dmg);
        if(getHP() <= 0)
        {
            setHP(0);
            onDeath();
        }
        apm.transform.FindChild("Healthbar").gameObject.GetComponent<Slider>().value = getHP();
    }

    public Vector2 getKnockbackVelocity()
    {
        return this.knockbackVelocity;
    }
    // Update is called once per frame
    protected override void Update ()
    {
        base.Update();

        
        if (Input.GetKey("t"))
        {
            Time.timeScale = 3;

        }
        else
        {
            Time.timeScale = 1;
        }
        

        Vector2 dir = new Vector2(0, 0);
        dir.x += Input.GetAxisRaw(playerPrefix + "Horizontal");
        dir.y += Input.GetAxisRaw(playerPrefix + "Vertical");

        this.move(dir);
        if (Input.GetButtonDown(playerPrefix + "Interact"))
        {
            //TODO: Set up item pickup w/new items/weapons
        }
        
    }

    public Color getColor()
    {
        return Color.white; //TODO: Make this actually work
    }

    public Vector2 getShootDirection()
    {
        if(playerPrefix == null || playerPrefix == "")
        {
            Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Vector2 dir = new Vector2(mouse.x - transform.position.x, mouse.y - transform.position.y);
            return dir;
        }else
        {
            float xDir = Input.GetAxisRaw(playerPrefix + "_Fire1");
            float yDir = Input.GetAxisRaw(playerPrefix + "_Fire2");
            Vector2 dir = new Vector2(Mathf.Round(xDir * 4), Mathf.Round(yDir * 4));
            return dir.normalized;
        }
        
    }


}
