﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Projectile : MonoBehaviour {
    [SerializeField]
    protected int damage;
    bool pierce = false;

    protected float knockbackMagnitude;



    public static GameObject spawnProjectile<T>(int damage, string spriteloc, float scale,
        Vector2 location, Vector2 velocity, float knockbackMagnitude = 0f, 
        bool pierce = false, InitializeDelegate init = null) where T : Projectile
    {


        float angle = Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        GameObject obj = new GameObject();

        obj.layer = LayerMask.NameToLayer("Projectiles");

        obj.transform.position = location;
        obj.transform.rotation = rotation;
        obj.AddComponent<T>();
        obj.name = "Projectile " + spriteloc + "_" + damage;
        if(init != null)
        {
            init();
        }
        obj.GetComponent<T>().damage = damage;
        obj.GetComponent<T>().pierce = pierce;
        obj.GetComponent<T>().knockbackMagnitude = knockbackMagnitude;





        obj.AddComponent<Rigidbody2D>();
        obj.GetComponent<Rigidbody2D>().velocity = velocity;

        obj.transform.localScale = new Vector3(scale, scale, scale);

        SpriteRenderer sr = obj.AddComponent<SpriteRenderer>();

        sr.sprite = Resources.Load<Sprite>(spriteloc) ;

        BoxCollider2D c2d = obj.AddComponent<BoxCollider2D>();
        c2d.size = sr.sprite.bounds.size;
        c2d.isTrigger = true;

        

        return obj;
    }

    public delegate void InitializeDelegate();

	public int getDamage()
    {
        return damage;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        


        Entity ent = collision.gameObject.GetComponent<Entity>();
        if (ent != null)
        {
            ent.damage(damage);
            if (!pierce) Destroy(this.gameObject);

            if(knockbackMagnitude != 0)
            {
                Vector2 knockback = this.GetComponent<Rigidbody2D>().velocity;
                knockback.Normalize();
                ent.knockback(knockback, knockbackMagnitude);
            }

        }else
        {
            if (collision.gameObject.transform.parent != null && collision.gameObject.transform.parent.name == "SpriteTileColliders")
            {
                Destroy(this.gameObject);
            }
        }

    }


}
