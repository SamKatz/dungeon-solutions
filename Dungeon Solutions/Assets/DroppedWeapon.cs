﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DroppedWeapon : MonoBehaviour {

    public float timeDropped;

    public const float weaponScale = 1;
    public const float itemsize = 3;


    public WeaponDetails wd;

    public static DroppedWeapon dropNewWeapon(WeaponDetails wd, Vector2 loc)
    {
        
        GameObject obj = new GameObject();
        obj.name = wd.toString();
        obj.layer = LayerMask.NameToLayer("Dropped Items");
        DroppedWeapon dw = obj.AddComponent<DroppedWeapon>();
        dw.timeDropped = Time.fixedTime;
        dw.wd = wd;

        obj.transform.position = loc;
        obj.transform.localScale = new Vector3(weaponScale, weaponScale, weaponScale);

        SpriteRenderer sr = obj.AddComponent<SpriteRenderer>();
        sr.sprite = Resources.Load<Sprite>(wd.spriteloc);

        obj.transform.localScale = wd.getWeaponScalar();

        BoxCollider2D c2d = obj.AddComponent<BoxCollider2D>();
        c2d.size = sr.sprite.bounds.size;
        c2d.isTrigger = true;

        Light lt = obj.AddComponent<Light>();

        
        /*Behaviour halo = (Behaviour)obj.GetComponent("Halo");
        halo.enabled = true;*/
        
        lt.range = 3;
        lt.color = wd.getColor();

        GameObject canvasobj = new GameObject();
        Canvas canvas = canvasobj.AddComponent<Canvas>();
        canvasobj.name = "Dropped Weapon UI";

        canvasobj.transform.SetParent(dw.gameObject.transform);
        canvas.renderMode = RenderMode.WorldSpace;
        Text text = canvasobj.AddComponent<Text>();
        canvasobj.transform.position = dw.transform.position;
        RectTransform rt = canvasobj.GetComponent<RectTransform>();
        rt.position = dw.transform.position;
        rt.sizeDelta = new Vector2(5, 5);

        text.text = dw.wd.type + "\\n" + "Damage: " + dw.wd.damage + "\\n" + dw.wd + "Knockback: " + dw.wd.knock;


        return dw;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        {
            Debug.Log("You're standing on a weapon with details: " + wd.ToString());
        }

    }


     // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    
    void Update () {
        

	}
}
