﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class AbilityPanelManager : MonoBehaviour {
    //temp
    //
    const int maxAbilities = 3;

    public List<Ability> AbilitysList = new List<Ability>(); //replace this with a proper query
    Ability currentAbility = null;
    [SerializeField]
    int AbilityNumber = -1;

    public Player player;
    [SerializeField]
    public string playerprefix = "";


    void Start()
    {
        if (player == null)
        {
            foreach (Player p in Player.players)
            {
                if (p.playerPrefix == playerprefix)
                {
                    player = p;
                    p.apm = this;
                    WeaponDetails wd = new WeaponDetails(WeaponDetails.WeaponType.Sword, false, 30, 20, "TestResources/testsword", WeaponDetails.Rarity.Rare);
                    WeaponDetails wd2 = new WeaponDetails(WeaponDetails.WeaponType.Wand, true, 10, 40, "TestResources/Copper_Broadsword", WeaponDetails.Rarity.Uncommon);
                    AbilitysList.Add(new RangedAbility(p, wd2));
                    AbilitysList.Add(new SapperAbility(p));
                    
                    updateAbilityDisplay();
                }
            }
        }
    }

    //return true if all abilities were interrupted sucessfully - return false if one was uninterruptable.
    //@SAM: Should this be a list of uninterruptible abilities? Might be more useful that way.
    public bool interruptAllAbilitiesExcept(int dontInterrupt)
    {
        bool allow = true;
        for (int j = 0; j < AbilitysList.Count; j++)
        {
            if (dontInterrupt == j)
            {
                continue;
            }
            if (!AbilitysList[j].interrupt())
            {
                allow = false;
            }
        }
        return allow;
    }

    void Update()
    {
        foreach (Ability s in AbilitysList)
        {
            s.update();
        }

        for (int i = 0; i < maxAbilities; i++)
        {

            if (i >= AbilitysList.Count) break;
            Ability s = AbilitysList[i];

            if (Input.GetButtonDown(playerprefix + "Ability " + (i + 1)))
            {

                if (this.AbilityNumber == i)
                {
                    this.deselectCurrentAbility();
                }
                else
                {
                    if (!interruptAllAbilitiesExcept(i)) continue;

                    this.deselectCurrentAbility();
                    this.selectAbility(i + 1);
                }


            }
            if (Input.GetButtonUp(playerprefix + "Ability " + (i + 1)))
            {
                AbilitysList[i].AbilitySelectButtonReleased();
            }



            bool controllerDirectionActive = (player.getShootDirection().SqrMagnitude() > .25);
            if (((Input.GetButtonDown("Fire1") && (this.player.playerPrefix == "")) || (controllerDirectionActive && this.player.playerPrefix != "")) && AbilitysList[i].acceptInput())
            {
                AbilitysList[i].directionClicked();
            }

            if (((Input.GetButtonUp("Fire1") && (this.player.playerPrefix == "")) || (!controllerDirectionActive && this.player.playerPrefix != ""))) //probably doesn't need a default usability check
            {
                AbilitysList[i].directionReleased();
            }

            Image thumb = this.transform.FindChild("Ability Thumbnail " + (i + 1)).gameObject.GetComponent<Image>();

            float fill = (Time.fixedTime - s.getLastUsed()) / s.getCooldown();
            if (fill > 1) fill = 1;
            else if (fill < 0) fill = 0;

            thumb.fillAmount = fill;
            if (fill != 1)
            {
                thumb.color = Color.grey;
            } else
            {
                thumb.color = Color.white;
            }

        }

        

        




        Slider health = this.transform.FindChild("Healthbar").gameObject.GetComponent<Slider>();
        health.maxValue = player.getHPMax();
        health.value = player.getHP();

    }

    public Ability getCurrentAbility()
    {
        return currentAbility;
    }

    public void updateAbilityDisplay()
    {
        for(int i = 0; i < maxAbilities; i++)
        {
            GameObject thumb = this.transform.FindChild("Ability Thumbnail " + (i + 1)).gameObject;
            if (i >= AbilitysList.Count)
            {
                thumb.SetActive(false);
            }else
            {
                thumb.SetActive(true);
                thumb.GetComponent<Image>().sprite = Resources.Load<Sprite>(AbilitysList[i].getSpriteName());
                Text label = this.gameObject.transform.FindChild("Ability Label " + (i + 1)).GetComponent<Text>();
                label.text = AbilitysList[i].getName();
               
            }
            

        }
    }

    public void deselectCurrentAbility()
    {
        if (currentAbility != null) //deselect Ability
        {
            GameObject thumb = this.transform.FindChild("Ability Thumbnail " + (this.AbilityNumber + 1)).gameObject;
            Text label = this.gameObject.transform.FindChild("Ability Label " + (AbilityNumber + 1)).GetComponent<Text>();
            label.color = Color.grey;

            currentAbility.AbilityDeselected();
            currentAbility = null;
            

            

            AbilityNumber = -1;
        }
    }

    public void selectAbility(int abilityNum)
    {
        
        if (AbilitysList.Count >= abilityNum - 1) //select Ability
        {
            currentAbility = AbilitysList[abilityNum - 1];
            AbilityNumber = abilityNum - 1;

            GameObject thumb = this.transform.FindChild("Ability Thumbnail " + abilityNum).gameObject;
            Text label = this.gameObject.transform.FindChild("Ability Label " + abilityNum).GetComponent<Text>();
            label.color = Color.black;

            currentAbility.AbilitySelected();

            
        }
    }

}
