﻿using UnityEngine;
using System.Collections;

public abstract class Ability
{


    protected float lastUsed = -10000;
    public void setLastUsed(float time) { lastUsed = time; }
    public float getLastUsed() { return lastUsed; }

    protected float cooldown = 0;
    public float getCooldown() { return cooldown; }

    public Player owner;

    public Ability(Player owner)
    {
        this.owner = owner;
    }


    // Use this for initialization - not a regular unity method
    public virtual void start()
    {

    }

    // Update is called once per frame - not a regular unity method
    public virtual void update()
    {
        
    }

    public virtual void AbilitySelected()
    {
    }

    public virtual void AbilitySelectButtonReleased()
    {

    }

    public virtual void AbilityDeselected()
    {
    }

    public virtual void directionClicked()
    {
        if (!acceptInput()) return;
    }

    public virtual void directionReleased()
    {
        if (!acceptInput()) return;
    }
    
    public virtual bool acceptInput() //by default, only accept input when the skill is selected and when it is not on cooldown
    {
        return owner.apm.getCurrentAbility() == this && this.usable();
    }



    /* Should define behavior to allow an ability to be interrupted by another ability - performing cleanup and similar.
     * Return true to allow the interrupt to take place and allow the player to switch to using a different ability.
     * Return false to prevent the interrupt from taking place and prevent the player form switching to a different ability.
     */
    public virtual bool interrupt()
    {
        if(owner.apm.getCurrentAbility() == this)
        {
            owner.apm.deselectCurrentAbility();
        }

        return true;
    }

    public virtual bool usable()
    {
        if (Time.fixedTime - this.lastUsed >= cooldown)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public abstract string getSpriteName();

    public abstract string getName();

    public int getIndex()
    {
        for(int i = 0; i < owner.apm.AbilitysList.Count; i++)
        {
            if (owner.apm.AbilitysList[i] == this) return i;
        }

        throw new System.InvalidOperationException("Ability not in its owner's ability list");
    }
}
