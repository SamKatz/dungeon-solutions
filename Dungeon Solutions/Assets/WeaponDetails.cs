﻿using UnityEngine;
using System.Collections;

public class WeaponDetails{

    public WeaponType type = WeaponType.Sword;

    public int damage = 9;

    public float knock = 200;

    public string spriteloc = "";

    public Rarity rarity = Rarity.Common;

    public bool ranged = false;

    public WeaponAbility ability = WeaponAbility.None;

    public WeaponDetails(WeaponType type, bool range, int damage, float knockback, string spriteloc, Rarity r = Rarity.Common, WeaponAbility wa = WeaponAbility.None) {
        this.type = type;
        this.damage = damage;
        this.spriteloc = spriteloc;
        this.knock = knockback;
        this.rarity = r;
        this.ability = wa;
        this.ranged = range;
    }

    public enum Rarity
    {
        Common,
        Uncommon,
        Rare
    }

    public enum WeaponType
    {
        Sword,
        Spear,
        Mace,
        Wand
    }

    public enum WeaponAbility
    {
        None, Savage
    }

    public static Vector2 getWeaponScalar(WeaponType type)
    {
        switch (type)
        {
            case WeaponType.Sword:
                return new Vector2(4, 4);
            case WeaponType.Wand:
                return new Vector2(6, 6);
            
            
            
        }
        throw new System.InvalidOperationException("Weapon type unimplmented.");
    }

    public static string getAbilityDescription(WeaponAbility wa)
    {
        switch (wa)
        {
            case WeaponAbility.None:
                return "";
            case WeaponAbility.Savage:
                return "Receives a +10% bonus to savageness or something.";
        }
        throw new System.InvalidOperationException("Weapon ability unimplmented.");
    }

    public Vector3 getWeaponScalar()
    {
        return getWeaponScalar(this.type);
    }

    public Color getColor()
    {
        return getRarityColor(this.rarity);
    }

    public static Color getRarityColor(Rarity r)
    {
        switch (r) {
            case Rarity.Common:
                return Color.white;
            case Rarity.Uncommon:
                return Color.green;
            case Rarity.Rare:
                return Color.blue;
        }
        throw new System.NotImplementedException("No listed color for rarity " + r.ToString());
    }

    public string toString()
    {
        string desc = "";
        if (ability != WeaponAbility.None) desc += ability.ToString();
        desc += " " + this.type.ToString();
        desc += "\\n";
        desc += getAbilityDescription(this.ability);
        return desc;
    }
}
