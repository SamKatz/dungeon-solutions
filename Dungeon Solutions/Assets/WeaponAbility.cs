﻿using UnityEngine;
using System.Collections;
using System;

public abstract class WeaponAbility : Ability
{
    public WeaponDetails weapon;

    public WeaponAbility(Player owner, WeaponDetails weapon) : base(owner)
    {
        this.weapon = weapon;
    }

    public override bool acceptInput()
    {
        return owner.apm.getCurrentAbility() == null && this.usable();
    }

    public override string getName()
    {
        bool useDetails = (weapon.ability.ToString() != WeaponDetails.WeaponAbility.None.ToString());

        if (useDetails)
        {
            return weapon.ability.ToString() + " " + weapon.type.ToString();
        }
        else
        {
            return weapon.type.ToString();
        }


    }

}
