﻿using UnityEngine;
using System.Collections;
using System;

public class BulwarkAbility : Ability
{
    

    public BulwarkAbility(Player owner) : base(owner)
    {
        this.cooldown = 5f;
    }

    protected GameObject deployedBarricade = null;
    protected int health = 100;

    public class BulwarkBarricade : MonoBehaviour
    {
        public BulwarkAbility ability = null;

        public void OnTriggerEnter2D(Collider2D other)
        {
            Projectile proj = other.gameObject.GetComponent<Projectile>();
            if (proj != null)
            {
                ability.health -= proj.getDamage();
                if(ability.health <= 0)
                {
                    Destroy(this.gameObject);
                    ability.lastUsed = Time.fixedTime;
                    

                }

                Destroy(other.gameObject);
            }
        }
    }

    public override void AbilitySelected()
    {
        base.AbilitySelected();
        if (deployedBarricade == null) return;
        else if (Vector3.Distance(owner.transform.position, deployedBarricade.transform.position) < 3)
        {
            GameObject.Destroy(deployedBarricade.gameObject);
            deployedBarricade = null;

            owner.apm.deselectCurrentAbility();
        }else
        {
            owner.apm.deselectCurrentAbility();
        }

    }


    public override void directionClicked()
    {
        if (!acceptInput()) return;
        if (deployedBarricade != null) return;

        GameObject barricade = new GameObject();
        barricade.name = "Bulwark Barricade";

        Vector2 facedir = owner.getShootDirection().normalized;

        barricade.transform.position = owner.transform.position + VectorUtils.to3d(facedir);
        barricade.transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(facedir.y, facedir.x) * Mathf.Rad2Deg + 90, Vector3.forward);
        SpriteRenderer sr = barricade.AddComponent<SpriteRenderer>();
        sr.sprite = Resources.Load<Sprite>("TestResources/wall");
        barricade.transform.localScale = new Vector3(2, 0.25f, 1);
        BoxCollider2D b2d = barricade.AddComponent<BoxCollider2D>();
        b2d.size = sr.sprite.bounds.size;
        b2d.isTrigger = true;

        barricade.AddComponent<BulwarkBarricade>();
        barricade.GetComponent<BulwarkBarricade>().ability = this;



        this.deployedBarricade = barricade;



        this.lastUsed = Time.fixedTime;

        owner.apm.deselectCurrentAbility();


    }

    public override string getName()
    {
        return "Deploy Barricade";
    }

    public override string getSpriteName()
    {
        return "TestResources/wall";
    }
}
