﻿using UnityEngine;
using System.Collections;
using System;
using SpriteTile;

public class SapperAbility : Ability {

    public const int damage = 5;
    public const float knockbackMagnitude = 15;

    public SapperAbility(Player owner) : base(owner)
    {
    }

    GameObject sapperCharge = null;

    public override void AbilitySelected()
    {
        base.AbilitySelected();
        if(sapperCharge != null)
        {
            sapperCharge.GetComponent<SapperCharge>().explode();
        }else
        {
            
            GameObject charge = new GameObject();
            charge.transform.position = owner.transform.position;
            SpriteRenderer sr = charge.AddComponent<SpriteRenderer>();
            sr.sprite = Resources.Load<Sprite>("TestResources/Circle_Orange");
            charge.AddComponent<SapperCharge>();
            sapperCharge = charge;
        }
        owner.apm.deselectCurrentAbility();

    }


    public override string getName()
    {
        return "Sapper's Charge";
    }

    public override string getSpriteName()
    {
        return "TestResources/mine-explosion";
    }

    public class SapperCharge : MonoBehaviour
    {
        const int explosionRadius = 3;

        public void explode()
        {
            

            Vector3 center = this.transform.position;
            Debug.Log("Exploding charge at (" + center.x + ", " + center.y + ")");
            
            Int2 startpos = Tile.WorldToMapPosition(center) + new Int2(-1 * explosionRadius/2, -1 * explosionRadius/2);
            for(int i = 0; i < explosionRadius; i++)
            {
                if (startpos.x + i > Tile.GetMapSize().x) break;
                for (int j = 0; j < explosionRadius; j++)
                {
                    if (startpos.y + j > Tile.GetMapSize().y) break;

                    Tile.DeleteTile(startpos + new Int2(i, j), true);
                }
            }





            foreach (Collider2D hit in Physics2D.OverlapCircleAll(center, explosionRadius * 2, LayerMask.GetMask("Entities")))
            {
                Entity e = hit.gameObject.GetComponent<Entity>();
                if(e != null)
                {
                    e.damage(damage);
                    float blastImpulse = Vector3.Distance(e.transform.position, center);
                    blastImpulse = blastImpulse / explosionRadius / 2;
                    blastImpulse = 1 - blastImpulse;
                    Debug.Log(blastImpulse);
                    blastImpulse *= 4;
                    Debug.Log(blastImpulse);
                    e.knockback((e.transform.position - center), knockbackMagnitude * blastImpulse);
                }

            }

            Destroy(this.gameObject);
        }
    }


}
