﻿using UnityEngine;
using System.Collections;
using System;

public class SprintAbility : Ability
{
    int slowid = -1;

    public SprintAbility(Player owner) : base(owner)
    {
    }

    public override void AbilitySelected()
    {
        base.AbilitySelected();

        ConditionManager.Slowed sprint = new ConditionManager.Slowed(Time.fixedTime + 36000, 2.5f);

        owner.addCondition(sprint);
        slowid = sprint.conditionID;
    }

    public override void AbilityDeselected()
    {
        base.AbilityDeselected();
    }

    public override void AbilitySelectButtonReleased()
    {
        base.AbilitySelectButtonReleased();
        owner.removeCondition(slowid);

        if(owner.apm.getCurrentAbility() == this)
        {
            owner.apm.deselectCurrentAbility();
        }
    }

    public override bool interrupt()
    {
        owner.removeCondition(slowid);
        return base.interrupt();
    }

    public override string getName()
    {
        return "Sprint";
    }

    public override string getSpriteName()
    {
        return "TestResources/sprint";
    }
}
