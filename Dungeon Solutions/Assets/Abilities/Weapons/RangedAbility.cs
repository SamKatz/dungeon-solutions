﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class RangedAbility : WeaponAbility
{





    public RangedAbility(Player owner, WeaponDetails details) : base(owner, details)
    {
        this.weapon = details;
        this.cooldown = 0.5f;
    }



    public override void directionClicked()
    {
        base.directionClicked();
        fireProjectile();
    }

    public override void AbilitySelected()
    {

        base.AbilitySelected();
        owner.apm.deselectCurrentAbility();


        if (owner.playerPrefix == "")
        {

            fireProjectile();
        }
    }

    public void fireProjectile()
    {
        if (!owner.apm.interruptAllAbilitiesExcept(getIndex()))
        {
            return;
        }

        this.lastUsed = Time.fixedTime;
        Vector2 dir = owner.GetComponent<Player>().getShootDirection();
        dir.Normalize();

        GameObject pro = Projectile.spawnProjectile<Projectile>(
            (int)weapon.damage, weapon.spriteloc, 6,
            VectorUtils.to2d(owner.transform.position) + (3 * dir * owner.GetComponent<Entity>().getProjectileSpawnDistance()),
            dir * 36,
            5.0f //this prevents ranged kb from working
            );

        pro.transform.Rotate(new Vector3(0, 0, 90));
    }


    public override string getSpriteName()
    {
        return weapon.spriteloc;
    }
}
