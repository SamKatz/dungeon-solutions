﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SwordAbility : WeaponAbility {


    public SwordAbility(Player owner, WeaponDetails details) : base(owner, details)
    {
        this.weapon = details;
        this.cooldown = 0.5f;
    }

    

    public override void directionClicked()
    {
        base.directionClicked();
        swingSword();
    }

    public override void AbilitySelected() { 
    
        base.AbilitySelected();
        owner.apm.deselectCurrentAbility();
    
        
        if(owner.playerPrefix == "")
        {
            
            swingSword();
        }
    }

    public void swingSword()
    {
        if (!owner.apm.interruptAllAbilitiesExcept(getIndex()))
        {
            return;
        }


        this.lastUsed = Time.fixedTime;
        Vector2 dir;

        GameObject obj = new GameObject();

        dir = owner.GetComponent<Player>().getShootDirection();

        dir.Normalize();

        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis((angle + 345) % 360, Vector3.forward);


        obj.transform.parent = owner.transform;
        obj.transform.position = new Vector3(owner.transform.position.x + (2 * dir.x), owner.transform.position.y + (2 * dir.y), 0);
        obj.transform.rotation = rotation;
        obj.AddComponent<Sword>();
        obj.name = "Sword: " + weapon.toString();

        obj.GetComponent<Sword>().damage = weapon.damage;
        obj.GetComponent<Sword>().knockback = weapon.knock;


        obj.transform.localScale = new Vector3(weapon.getWeaponScalar()[0] / owner.transform.localScale.x,
                                            weapon.getWeaponScalar()[1] / owner.transform.localScale.y, 1);

        SpriteRenderer sr = obj.AddComponent<SpriteRenderer>();

        sr.sprite = Resources.Load<Sprite>(weapon.spriteloc);

        BoxCollider2D c2d = obj.AddComponent<BoxCollider2D>();
        c2d.size = sr.sprite.bounds.size;
        c2d.isTrigger = true;
    }

    public class Sword : MonoBehaviour
    {
        public int damage;
        public float knockback;
        protected List<Entity> hit = new List<Entity>();
       



        void OnTriggerEnter2D(Collider2D collision)
        {

            Entity ent = collision.gameObject.GetComponent<Entity>();
            if (ent != null && !ent.gameObject.transform.Equals(this.transform.parent) && !hit.Contains(ent))
            {
                ent.damage(damage);
                Vector2 dir = new Vector2(this.transform.position.x - ent.gameObject.transform.position.x, this.transform.position.y - ent.gameObject.transform.position.y);
                dir = dir.normalized;
                ent.knockback(-1 * dir, knockback);
                hit.Insert(0, ent);
            }
        }

        void Start()
        {
            Destroy(this.gameObject, 0.5f);
        }

        protected virtual void Update()
        {

            this.transform.Rotate(0, 0, -1 * Time.deltaTime * 60);
            this.transform.RotateAround(this.transform.parent.position, Vector3.forward, -2 * Time.deltaTime * 60);


        }
    }



    public override string getSpriteName()
    {
        return weapon.spriteloc;
    }

    public override bool interrupt()
    {
        //this removes all swords being swung and lets the player use other abilities
        foreach(Transform child in owner.gameObject.transform)
        {
            if (child.gameObject.name.StartsWith("Sword: "))
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        return true;
        //return usable();
        //this would make you only able to use other abilities when you had finished swinging your sword and could use it again
    }


}
