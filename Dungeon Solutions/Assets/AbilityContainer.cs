﻿using UnityEngine;
using System.Collections;

public class AbilityContainer : MonoBehaviour {

    protected Ability playerItem;

    public static AbilityContainer createAbilityContainer(Ability skill, float x, float y)
    {
        GameObject go = new GameObject("AbilityContainer_"+skill.getName());

        AbilityContainer ret = go.AddComponent<AbilityContainer>();
        ret.playerItem = skill;

        SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
        sr.sprite = Resources.Load<Sprite>(skill.getSpriteName());
        BoxCollider2D c2d = go.AddComponent<BoxCollider2D>();

        c2d.size = sr.sprite.bounds.size;
        c2d.isTrigger = true;

        go.transform.position = new Vector3(x, y, 0);
        go.transform.localScale = new Vector3(4, 4, 0);

        return ret;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {

        Player ent = collision.gameObject.GetComponent<Player>();
        if (ent != null)
        {
            setAbilityPlayer(ent);
            ent.apm.AbilitysList[0] = playerItem;
            ent.apm.updateAbilityDisplay();
        }
    }

    // Use this for initialization
    void Start ()
    {
	  
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void setAbilityPlayer(Player p)
    {
        WeaponDetails wd;
        try
        {
            wd = ((WeaponAbility)playerItem).weapon;
        }
        catch(System.Exception ex)
        {
            //this doesnt have to return if items and classes dont have weapondetails
            return;
        }

        switch(playerItem.GetType().ToString())
        {
            case "SwordAbility": playerItem = new SwordAbility(p, wd); break;
            case "RangedAbility": playerItem = new RangedAbility(p, wd); break;
            //add more weapons here
            default: playerItem = new SwordAbility(p, wd); break;
        }
    }
}
