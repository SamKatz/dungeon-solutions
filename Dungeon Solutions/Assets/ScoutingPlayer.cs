﻿using UnityEngine;
using System.Collections;

public class ScoutingPlayer : MonoBehaviour {

    public string playerPrefix;

    SpriteRenderer sr;

	// Use this for initialization
	void Start () {
        sr = this.gameObject.AddComponent<SpriteRenderer>();
        sr.sprite = Resources.Load<Sprite>("TestResources/whitecircle");

        gameObject.transform.localScale = new Vector3(0.25f, 0.25f, 1);
        //set color by prefix


	}
	
	// Update is called once per frame
	void Update () {
        this.gameObject.transform.position += new Vector3(
            Input.GetAxisRaw(playerPrefix + "Horizontal") * 10 * Time.deltaTime,
            Input.GetAxisRaw(playerPrefix + "Vertical") * 10 *  Time.deltaTime, 0
            );

	}
}
