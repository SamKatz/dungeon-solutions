﻿using UnityEngine;
using System.Collections;

public class AIUtils {

	public static bool canISeePlayer(Entity e)
    {
        foreach(Player p in Player.players)
        {

            int mask = LayerMask.GetMask("Level Geometry", "Entities");
            

            RaycastHit2D rh2d = Physics2D.Raycast(e.transform.position + (p.transform.position - e.transform.position).normalized * e.getProjectileSpawnDistance(), p.transform.position - e.transform.position, 1000, mask);

            //Debug.Log("Found " + rh2d.collider.gameObject.name);
            if (rh2d.collider != null && rh2d.collider.gameObject.GetComponent<Player>() != null)
            {
                return true;
            }
        }
        return false;
    }
}
