﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Entity : MonoBehaviour
{
    protected Rigidbody2D rigidBody;
    protected SpriteRenderer spriteObj;

    public static HashSet<Entity> entities = new HashSet<Entity>();
    
    [SerializeField]
    protected Vector2 moveVelocity;
    [SerializeField]
    protected Vector2 knockbackVelocity;

    public delegate void EntityDamagedEvent(EntityDamageInfo edi);
    public static EntityDamagedEvent onEntityDamaged;

    protected float speed;

    [SerializeField]
    protected int hitPoints;
    [SerializeField]
    protected int hitPointsMAX;

    [SerializeField]
    protected float projectileSpawnDistance;

    public float getProjectileSpawnDistance() { return projectileSpawnDistance; }


    public int getHP() { return hitPoints; }
    public int getHPMax() { return hitPointsMAX; }

    public virtual void setHPMax(int newHPMax)
    {
        if(newHPMax < hitPoints)
        {
            hitPoints = newHPMax;
        }
        hitPointsMAX = newHPMax;
    }

    public virtual void setHP(int newHP)
    {
        if(newHP <= 0)
            { hitPoints = 0; onDeath(); }
        else if(newHP > hitPointsMAX)
            { hitPoints = hitPointsMAX; }
        else
            hitPoints = newHP;
    }

    public virtual void damage(int dmg)
    {
        EntityDamageInfo info = new EntityDamageInfo(this, dmg);
        onEntityDamaged(info);
        if (info.cancelled) return;
        dmg = info.damage;


        setHP(hitPoints - dmg);


    }
    
    protected virtual void onDeath()
    {
        Entity.entities.Remove(this);

        this.removeAllConditions();

        Destroy(this.gameObject);
    }

    protected virtual void move(Vector3 dir)
    {
        move(new Vector2(dir.x, dir.y));
    }

    protected virtual void move(Vector2 dir)
    {
        if (!canIMove())
        {
            this.moveVelocity = new Vector2(0, 0);
            return;
        }

        this.moveVelocity = dir;
        this.moveVelocity.Normalize();
        this.moveVelocity *= speed;

        if (hasCondition<ConditionManager.Slowed>())
        {
            foreach(ConditionManager.Condition c in getConditions<ConditionManager.Slowed>())
            {
                ConditionManager.Slowed slow = (ConditionManager.Slowed) c;
                this.moveVelocity *= slow.multiplier;

            }
        }
    }

    public void attack(WeaponDetails weapon)
    {
        
    }

    public virtual void knockback(Vector2 impulse)
    {
        this.knockbackVelocity += impulse;
    }

    public virtual void knockback(Vector2 dir, float magnitude)
    {
        this.knockback(dir.normalized * magnitude);
    }

    // Use this for initialization
    protected virtual void Start ()
    {

        if (this.gameObject.GetComponent<Rigidbody2D>() == null) rigidBody = this.gameObject.AddComponent<Rigidbody2D>();
        else rigidBody = this.GetComponent<Rigidbody2D>();
        if (this.gameObject.GetComponent<SpriteRenderer>() == null) spriteObj = this.gameObject.AddComponent<SpriteRenderer>();
        else spriteObj = this.GetComponent<SpriteRenderer>();
        this.gameObject.layer = LayerMask.NameToLayer("Entities");
        Entity.entities.Add(this);

    }

    protected virtual void Awake()
    {

    }


    // Update is called once per frame
    protected virtual void Update()
    {
        //Debug.Log("Begin update cycle");
        

        if (knockbackVelocity.SqrMagnitude() < 64) knockbackVelocity = new Vector2(0, 0);
        else
        {
            knockbackVelocity -= .025f * knockbackVelocity * Time.deltaTime * 60;
        }

        if(knockbackVelocity.SqrMagnitude() > 0)
            this.rigidBody.velocity = knockbackVelocity;
        else
            this.rigidBody.velocity = moveVelocity;
        //Debug.Log("done with knockback stuff");
        //if (conditions.Count != 0) Debug.Log("Next condition to be removed " + conditions.Values[0].expirationTime + " and it is currently " + Time.fixedTime);

        for (int i = 0; i < conditions.Count; i++)
        {
            if(conditions[i].expirationTime <= Time.fixedTime)
            {
                conditions[i].markfordeletion = true;
                conditions[i].onConditionRemove(this);
            }
        }

        
        //Debug.Log("Begin updates");
        for (int i = 0; i < conditions.Count; i++)
        {
            ConditionManager.Condition c = conditions[i];
            if (!c.markfordeletion)
            {
                //Debug.Log("Updating " + c.GetType().Name);
                c.onConditionUpdate(this);
                //Debug.Log("Done Updating " + c.GetType().Name);
            }
            
            
        }
        //Debug.Log("end updates");
    }

    public virtual void LateUpdate()
    {
        //Debug.Log("late update");

        //Debug.Log("begin c mark removal");
        for(int i = 0; i < conditions.Count; i++)
        {
            if (conditions[i].markfordeletion)
            {
                conditions.RemoveAt(i);
                i--;
                continue;
            }

        }
        //Debug.Log("Done w/LateUpdate");
    }

    protected List<ConditionManager.Condition> conditions = new List<ConditionManager.Condition>();
    
    public bool hasCondition(int id)
    {
        for (int i = 0; i < conditions.Count; i++)
        {
            
            if (conditions[i].conditionID == id && !conditions[i].markfordeletion) return true;
            

            
        }
        return false;
    }

    public bool hasCondition<T>() where T : ConditionManager.Condition
    {
        for (int i = 0; i < conditions.Count; i++)
        {
            
            if (conditions[i] is T && !conditions[i].markfordeletion)
            {
                return true;
            }
            

                
        }
        return false;
    }

    

    public void addCondition(ConditionManager.Condition cond, bool doOnAdd = true)
    {
        if(cond.expirationTime < Time.fixedTime)
        {
            throw new InvalidOperationException("Attempted to add a condition that is set to expire before you added it.");
        }

        conditions.Add(cond);
        if(doOnAdd) cond.onConditionAdded(this);
    }

    public bool removeFirstCondition<T>() where T : ConditionManager.Condition //returns true if found
    {
        for(int i = 0; i < conditions.Count; i++)
        {
            
            if (conditions[i] is T)
            {
                conditions[i].onConditionRemove(this);
                conditions[i].markfordeletion = true;
                return true;
            }
            


                
        }
        return false;
    }

    public void removeAllConditions<T>() where T : ConditionManager.Condition
    {
        for (int i = 0; i < conditions.Count; i++)
        {
            
            if (conditions[i] is T)
            {
                conditions[i].onConditionRemove(this);
                conditions[i].markfordeletion = true;
                    
            }
            
        }
        

    }

    public void removeCondition(int id)
    {
        for (int i = 0; i < conditions.Count; i++)
        {
            
            if (conditions[i].conditionID == id)
            {
                conditions[i].onConditionRemove(this);
                conditions[i].markfordeletion = true;
                    
                return;
            }
            

            
        }

    }
    public void removeAllConditions()
    {
        for (int i = 0; i < conditions.Count; i++)
        {
            
                
            conditions[i].onConditionRemove(this);
            conditions[i].markfordeletion = true;
            


        }
        //conditions.Clear();
    }

    public List<ConditionManager.Condition> getConditions<T>() where T : ConditionManager.Condition
    {
        List<ConditionManager.Condition> result = new List<ConditionManager.Condition>();
        for (int i = 0; i < conditions.Count; i++)
        {
            
            if (conditions[i] is T && !conditions[i].markfordeletion) result.Add(conditions[i]);
                
                
                
            
        }
        return result;
    }

    public bool canIMove()
    {
        return !(hasCondition<ConditionManager.Stunned>());
    }

    public bool canIAttack()
    {
        return !(hasCondition<ConditionManager.Stunned>()) && !(hasCondition<ConditionManager.Terrified>());
    }

    public class EntityDamageInfo
    {
        public Entity entity;

        public int damage;

        public bool cancelled = false;

        public EntityDamageInfo(Entity entity, int damage)
        {
            this.entity = entity;
            this.damage = damage;
        }

    }
}