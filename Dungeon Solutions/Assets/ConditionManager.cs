﻿using UnityEngine;
using System.Collections;
using System;

public class ConditionManager : MonoBehaviour {

    public abstract class Condition : IComparable<Condition> {
        private static int lastUsedID = 0;
        public float expirationTime;
        public bool markfordeletion = false;
        public readonly int conditionID;
        public Condition(float expirationTime)
        {
            lastUsedID++;
            this.conditionID = lastUsedID;
            this.expirationTime = expirationTime;
        }

        public int CompareTo(Condition other)
        {
            return expirationTime.CompareTo(other.expirationTime);
        }

        public virtual void onConditionAdded(Entity e)
        {

        }

        public virtual void onConditionRemove(Entity e)
        {

        }

        public virtual void onConditionUpdate(Entity e)
        {

        }
    }

    public class Terrified : Condition
    {
        Entity terrifier;
        public Terrified(float expirationTime, Entity terrifier) : base(expirationTime)
        {
            this.terrifier = terrifier;
        }
    }

    public class Enraged : Condition
    {
        public Entity target;

        public Enraged(float expirationTime, Entity target) : base(expirationTime)
        {
            this.target = target;
        }

        public override void onConditionAdded(Entity e)
        {
            base.onConditionAdded(e);
            Enemy enemy = e.gameObject.GetComponent<Enemy>();
            if(enemy != null)
            {
                enemy.setTarget(target);
            }else
            {
                throw new InvalidOperationException("A entity that is not an Enemy cannot be enraged.");
            }
        }
    }

    public class Bubble : Condition
    {
        public Bubble(float expirationTime) : base(expirationTime) {}

        public override void onConditionAdded(Entity e)
        {
            base.onConditionAdded(e);
            Color tmp = Color.yellow;
            tmp.g = 0.25f;
            e.GetComponent<SpriteRenderer>().color = tmp;
        }

        public override void onConditionRemove(Entity e)
        {
            base.onConditionRemove(e);

            e.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    public class OnFire : Condition
    {


        public int magnitude;
        float lastDamaged;
        float interval;
        public OnFire(float expirationTime, int magnitude, int interval = 1) : base(expirationTime)
        {
            this.magnitude = magnitude;
            this.lastDamaged = Time.fixedTime;
            this.interval = interval;
        }

        public override void onConditionAdded(Entity e)
        {
            base.onConditionAdded(e);

            e.GetComponent<SpriteRenderer>().color = Color.red;
            e.damage(magnitude);
        }

        public override void onConditionRemove(Entity e)
        {
            base.onConditionRemove(e);

            e.GetComponent<SpriteRenderer>().color = Color.white;
        }

        public override void onConditionUpdate(Entity e)
        {
            base.onConditionUpdate(e);
            if(Time.fixedTime - this.lastDamaged > interval)
            {
                e.damage(magnitude);
                this.lastDamaged = this.lastDamaged + interval;
            }
        }

    }

    public class Stunned : Condition
    {
        public Stunned(float expirationTime) : base(expirationTime)
        {
        }
        public override void onConditionAdded(Entity e)
        {
            base.onConditionAdded(e);

            e.GetComponent<SpriteRenderer>().color = Color.cyan;
        }

        public override void onConditionRemove(Entity e)
        {
            base.onConditionRemove(e);

            e.GetComponent<SpriteRenderer>().color = Color.white;
        }

    }

    public class Slowed : Condition
    {
        public float multiplier;
        public Slowed(float expirationTime, float multiplier) : base(expirationTime)
        {
            this.multiplier = multiplier;
        }

        public override void onConditionAdded(Entity e)
        {
            base.onConditionAdded(e);

            //e.GetComponent<SpriteRenderer>().color = Color.blue;
        }

        public override void onConditionRemove(Entity e)
        {
            base.onConditionRemove(e);
            Color tmp = Color.yellow;

            //e.GetComponent<SpriteRenderer>().color = Color.white;
        }

    }

    public class Invincible : Condition
    {
        public Invincible(float expirationTime) : base(expirationTime)
        {
        }

        public override void onConditionAdded(Entity e)
        {
            base.onConditionAdded(e);

            e.GetComponent<SpriteRenderer>().color = Color.yellow;
        }

        public override void onConditionRemove(Entity e)
        {
            base.onConditionRemove(e);

            e.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
    // Use this for initialization
    void Start () {
        Entity.onEntityDamaged += HandleBubbleDamage;
        Entity.onEntityDamaged += HandleInvincibleDamage;
	}
	
    void HandleBubbleDamage(Entity.EntityDamageInfo edi)
    {
        if (edi.entity.hasCondition<Bubble>())
        {
            edi.cancelled = true;
            edi.entity.removeAllConditions<Bubble>();
            edi.entity.addCondition(new ConditionManager.Invincible(Time.fixedTime + 2f));
        }
    }

    void HandleInvincibleDamage(Entity.EntityDamageInfo edi)
    {
        if (edi.entity.hasCondition<Invincible>())
        {
            edi.cancelled = true;
        }
    }

}
