﻿using UnityEngine;
using System.Collections;


public class TriggerLava : MonoBehaviour {

	public void OnTriggerEnter2D(Collider2D c2d)
    {
        if(c2d.gameObject.GetComponent<Player>() != null)
        {
            TestTilemapLoader.doLava = true;
            Destroy(this.gameObject);
        }
    }
}
